
# After attach block volume 

ls -las /dev/oracleoci/oraclevdb*

# Create mount point
mkdir /u01

# Create partition
echo -e "n\np\n1\n\n\nw" | fdisk /dev/oracleoci/oraclevdb

echo "Disk partition created";
sleep 60

# Create Filesystem
mkfs -t ext4 /dev/oracleoci/oraclevdb1
echo "Disk filesystem created";

# Add fstab
echo UUID=\"`ls -l /dev/disk/by-uuid/ | grep sdb1 | awk '{print $9}'`\"   /u01    ext4       defaults,noatime,_netdev      0      2 >> /etc/fstab
mount /u01
echo "Disk Mounted";

