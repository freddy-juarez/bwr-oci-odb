#!/bin/bash

export ORACLE_BASE="/u01/oracle/Database"
export ORACLE_HOME="/u01/oracle/Database/product/19c/dbhome_1"
export ORACLE_SID="ORCL"
export ORACLE_PDB="INFRA"
export ORACLE_CHARACTERSET="AL32UTF8"
export ORACLE_EDITION="EE"
export SYSTEM_TIMEZONE="America/Mexico_City"

export ORACLE_PWD="welcome1"
export SOFTWARE=/home/opc/V982063-01.zip

if [ -f "$SOFTWARE" ]; then
    echo "$SOFTWARE will be install"
else 
    echo "$SOFTWARE does not exist, it is required."
    exit;
fi

echo 'Load variables:'
echo 'ORACLE_BASE' : $ORACLE_BASE
echo 'ORACLE_HOME' : $ORACLE_HOME
echo 'ORACLE_SID' : $ORACLE_SID
echo 'ORACLE_PDB' : $ORACLE_PDB
echo 'ORACLE_CHARACTERSET' : $ORACLE_CHARACTERSET
echo 'ORACLE_EDITION' : $ORACLE_EDITION
echo 'SYSTEM_TIMEZONE' : $SYSTEM_TIMEZONE
echo ''

echo 'INSTALLER: Started up'
echo ''

# get up to date
yum upgrade -y 
echo 'INSTALLER: System updated'
echo ''

# fix locale warning
yum reinstall -y glibc-common
echo LANG=en_US.utf-8 >> /etc/environment
echo LC_ALL=en_US.utf-8 >> /etc/environment
echo 'INSTALLER: Locale set'
echo ''

# set system time zone
sudo timedatectl set-timezone $SYSTEM_TIMEZONE
echo "INSTALLER: System time zone set to $SYSTEM_TIMEZONE"
echo ''

# Install Oracle Database prereq and openssl packages
yum install -y oracle-database-preinstall-19c openssl
echo 'INSTALLER: Oracle preinstall and openssl complete'
echo ''

# create directories
mkdir -p $ORACLE_HOME && \
chown -R oracle:oinstall /u01 && \
mkdir -p /u01/app && \
ln -s $ORACLE_BASE /u01/app/oracle
echo 'INSTALLER: Oracle directories created'
echo ''

# set environment variables
echo "export ORACLE_BASE=$ORACLE_BASE" >> /home/oracle/.bashrc && \
echo "export ORACLE_HOME=$ORACLE_HOME" >> /home/oracle/.bashrc && \
echo "export ORACLE_SID=$ORACLE_SID" >> /home/oracle/.bashrc   && \
echo "export PATH=\$PATH:\$ORACLE_HOME/bin" >> /home/oracle/.bashrc
echo 'INSTALLER: Environment variables set'
echo ''

# Install Oracle
unzip $SOFTWARE -d $ORACLE_HOME/
cp -v ../ora-response/db_install.rsp.tmpl /tmp/db_install.rsp
sed -i -e "s|###ORACLE_BASE###|$ORACLE_BASE|g" /tmp/db_install.rsp && \
sed -i -e "s|###ORACLE_HOME###|$ORACLE_HOME|g" /tmp/db_install.rsp && \
sed -i -e "s|###ORACLE_EDITION###|$ORACLE_EDITION|g" /tmp/db_install.rsp && \
chown oracle:oinstall -R $ORACLE_BASE

su -l oracle -c "yes | $ORACLE_HOME/runInstaller -silent -ignorePrereqFailure -waitforcompletion -responseFile /tmp/db_install.rsp"
$ORACLE_BASE/oraInventory/orainstRoot.sh
$ORACLE_HOME/root.sh
echo 'INSTALLER: Oracle software installed'
echo ''

# create sqlnet.ora, listener.ora and tnsnames.ora
su -l oracle -c "mkdir -p $ORACLE_HOME/network/admin"
su -l oracle -c "echo 'NAME.DIRECTORY_PATH= (TNSNAMES, EZCONNECT, HOSTNAME)' > $ORACLE_HOME/network/admin/sqlnet.ora"

# Listener.ora
su -l oracle -c "echo 'LISTENER = 
(DESCRIPTION_LIST = 
  (DESCRIPTION = 
    (ADDRESS = (PROTOCOL = IPC)(KEY = EXTPROC1)) 
    (ADDRESS = (PROTOCOL = TCP)(HOST = 0.0.0.0)(PORT = 1521)) 
  ) 
) 

DEDICATED_THROUGH_BROKER_LISTENER=ON
DIAG_ADR_ENABLED = off
' > $ORACLE_HOME/network/admin/listener.ora"

su -l oracle -c "echo '$ORACLE_SID=localhost:1521/$ORACLE_SID' > $ORACLE_HOME/network/admin/tnsnames.ora"
su -l oracle -c "echo '$ORACLE_PDB= 
(DESCRIPTION = 
  (ADDRESS = (PROTOCOL = TCP)(HOST = 0.0.0.0)(PORT = 1521))
  (CONNECT_DATA =
    (SERVER = DEDICATED)
    (SERVICE_NAME = $ORACLE_PDB)
  )
)' >> $ORACLE_HOME/network/admin/tnsnames.ora"

# Start LISTENER
su -l oracle -c "lsnrctl start"
echo 'INSTALLER: Listener created'
echo ''

# Create database
cp ../ora-response/dbca.rsp.tmpl /tmp/dbca.rsp
sed -i -e "s|###ORACLE_SID###|$ORACLE_SID|g" /tmp/dbca.rsp && \
sed -i -e "s|###ORACLE_PDB###|$ORACLE_PDB|g" /tmp/dbca.rsp && \
sed -i -e "s|###ORACLE_CHARACTERSET###|$ORACLE_CHARACTERSET|g" /tmp/dbca.rsp && \
sed -i -e "s|###ORACLE_PWD###|$ORACLE_PWD|g" /tmp/dbca.rsp

# Create DB
su -l oracle -c "dbca -silent -createDatabase -responseFile /tmp/dbca.rsp"

# Post DB setup tasks
su -l oracle -c "sqlplus / as sysdba <<EOF
   ALTER PLUGGABLE DATABASE $ORACLE_PDB SAVE STATE;
   EXEC DBMS_XDB_CONFIG.SETGLOBALPORTENABLED (TRUE);
   exit;
EOF"
echo 'INSTALLER: Database created'
echo ''

sed '$s/N/Y/' /etc/oratab | sudo tee /etc/oratab > /dev/null
echo 'INSTALLER: Oratab configured'
echo ''

# configure systemd to start oracle instance on startup
sudo cp ../scripts/oracle-rdbms.service /etc/systemd/system/
sudo sed -i -e "s|###ORACLE_HOME###|$ORACLE_HOME|g" /etc/systemd/system/oracle-rdbms.service
sudo systemctl daemon-reload
sudo systemctl enable oracle-rdbms
sudo systemctl start oracle-rdbms
echo "INSTALLER: Created and enabled oracle-rdbms systemd's service"

# Add rules firewall
firewall-cmd --zone=public --permanent --add-port=1521/tcp
firewall-cmd --zone=public --permanent --add-port=5500/tcp
firewall-cmd --reload
echo "Firewall rules added";
echo ''

echo "ORACLE PASSWORD FOR SYS, SYSTEM AND PDBADMIN: $ORACLE_PWD";

echo "INSTALLER: Installation complete, database ready to use!";
